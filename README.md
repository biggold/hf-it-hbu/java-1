# Java 1 Modul @HBU
## Repository Aufbau
* docs/ -> Enthält die Kursunterlagen.
* src/ -> Enthält mein Java source code.

## Über das Modul
https://www.hbu.ch/de/-internes-/Kursdetail-Emfpangsbestaetigung/Programmieren_Java_1.kHF-JA1.10098.html
### Inhalt
* Einführung in Java Technologie und virtuelle Maschinen
* Grundelemente der Programmiersprache, wie Anweisungen, Ausdrücke, Operatoren und Kontrollstrukturen
* Speichermodell (Heap/Stack)
* Einfache und zusammengesetzte Datentypen, Referenzmodell
* Ein- und mehrdimensionale Arrays
* Methoden und Blöcke
* Kurzeinführung in die Notationssprache UML
* Objektorientierte Programmierung Klassen, Referenzen und Objekte
* Klassenvariablen und Klassenmethoden
* Vererbung und Polymorphismus Zugriffsmodifikatoren und Pakete
* Wichtige Klassen der Klassenbibliothek (z.B. String, java.lang.Math usw.)
* Übersicht der Java-Zertifizierungen


### Ziele
* Sie verstehen die grundlegende Arbeitsweise eines Computerprogramms.
* Sie verstehen die allgemeinen Grundlagen der Programmierung unabhängig von einer Programmiersprache.
* Sie kennen die Sprachelemente der Programmiersprache Java.
* Sie kennen die wesentlichsten Elemente der Systemarchitektur von Java und verstehen diese.
* Sie sind in der Lage mit Hilfe der Programmiersprache Java in einer ausgewählten Entwicklungsumgebung einfache Softwaremodule zu programmieren.