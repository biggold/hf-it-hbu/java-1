package abend2.uebung6;



public class UeberlaufDatentypen {
    public static void main(String[] args){
        short wert1 = 30000;
        short wert2 = 30000;
        short result;
        System.out.println(Short.MAX_VALUE);
        System.out.println(Short.MIN_VALUE);
        result = (short) (wert1 + wert2);
        System.out.println(result); // -5536 -> Binär rechnen

        float f = 6999.9F;
    }
}