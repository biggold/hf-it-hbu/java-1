package abend2.uebung3;

import java.util.Scanner;  // Import the Scanner class

/** Aufgabenstellung
 * Schreibe ein Java Programm das die Oberfläche einer Kugel berechnet. Eingabeparameter ist der
 * Durchmesser einer Kugel, Ausgabe sollte die berechnete Oberfläche sein.
 * OberflächeKugel = PI * d^2
 * Als Vorlage kann das Beispiel zur Temperaturumrechnung aus den Folien verwendet werden.
 */

public class Kubeloberflaeche {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object


        double durchmesser;
        // Enter username and press Enter
        System.out.print("Kubel Durchmesser eingeben: ");
        if (scanner.hasNextDouble()) {
            durchmesser = scanner.nextFloat();
            // Calculate
            double flaeche;
            flaeche = Math.PI * Math.pow(durchmesser, 2);
            System.out.println("Kugel Oberfläche: " + flaeche);
        } else {
            System.out.println("Please enter a double value! Format: 2354.213");
            System.exit(1);
        }
    }
}
