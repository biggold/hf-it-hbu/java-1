package abend2.uebung1;

/** Aufgabenstellung
 * Finde für die folgenden Angaben den passenden (primitiven) Datentyp.
 * • Die Entfernung bei Laufdisziplinen bei Olympischen Spielen
 * • Das Gewicht eines Mehlsackes
 * • Das Alter eines Schulkindes
 * • Ein Bankkontosaldo
 * • Eine Autonummer
 * Schreibe für jedes Beispiel eine Zeile Java Code, welche eine Variable des entsprechenden Datentyps
 * deklariert, mit einem gültigen Wert initialisiert und anschliessend eine Zeile in der Console ausgibt.
 *
 * Beispiel:
 *      int dist; // Deklaration
 *      dist = 100; // Initialisierung
 *      System.out.println(′′Distanz = ′′ + dist); // Console output
 */

public class PassendeDatentypen {
    public static void main(String[] args){
        float distance; // Die Entfernung bei Laufdisziplinen bei Olympischen Spielen
        float weight; // Das Gewicht eines Mehlsackes
        byte age; // Das Alter eines Schulkindes
        double saldo; // Ein Bankkontosaldo
        char carSign; // Eine Autonummer
        System.out.println(("Hello World!"));
    }
}
