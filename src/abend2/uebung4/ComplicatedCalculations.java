package abend2.uebung4;

/** Aufgabenstellung
 * Berechne die folgenden Beispiele zuerst von Hand und überprüfe deine Lösung nachher im Code. Es wird
 * immer mit int-Werten gerechnet.
 * Wert a   Wert b    Wert c        Ausdruck
 * 20       15        15            a / b + c
 * 20       15        3             a % b % c
 * 20       15        2             a++ + b * c
 * 16       2         2             a += b + c
 * 8        8         8             a == b
 * 8        12        8             a > b - c
 */

public class ComplicatedCalculations {
    public static void main(String[] args){
        int a = 20;
        int b = 15;
        int c = 15;
        System.out.println(a / b + c); // 16.33 -> 16 da int
        c = 3;
        System.out.println(a % b % c); // 2
        c = 2;
        System.out.println(a++ + b * c); // 50
        a = 16;
        b = 2;
        c = 2;
        System.out.println(a += b + c); // 20
        a = 8;
        b = 8;
        c = 8;
        System.out.println(a == b); // True
        a = 8;
        b = 12;
        c = 8;
        System.out.println(a > b - c); // True
    }
}
