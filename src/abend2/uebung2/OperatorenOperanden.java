package abend2.uebung2;

/** Aufgabenstellung
 * Berechne die folgenden Beispiele zuerst von Hand und überprüfe deine Lösung nachher im Code. Es wird
 * immer mit int-Werten gerechnet.
 * Wert a   Wert b  Ausdruck
 * 20       15      a / b
 * 20       15      a % b
 * 20       15      a++ + b
 * 16       2       a += b
 * 8        8       a == b
 * 8        8       a > b
 * 12       3       a & b
 * 12       3       a | b
 * Berechne die folgenden Beispiele zuerst von Hand und überprüfe deine Lösung nachher im Code. Es wird
 * immer mit boolean-Werten gerechnet.
 * Wert a   Wert b  Ausdruck
 * true     false   a && b
 * true     false   a ^ b
 */

public class OperatorenOperanden {
    public static void main(String[] args){
        int a,b; // Value a & b to calculate
        a = 20;
        b = 15;
        System.out.println(a/b); // 1.333
        System.out.println(a%b); // 5
        System.out.println(a++ + b); // 35 -> a++ gets counted +1 after calculation
        a = 16;
        b = 2;
        System.out.println(a += b); // 18
        a = 8;
        b = 8;
        System.out.println(a == b); // True
        System.out.println(a > b); // False
        a = 12;
        b = 3;
        System.out.println(a & b); // 0 -> Bitwise comparison AND, example: 0b10111011 = 0b10111111 & 0b11111011
        System.out.println(a | b); // 15 -> Bitwise comparison OR, example: 0b10111011 = 0b10001000 | 0b00111011

        boolean x,y;
        y = true;
        x = false;
        System.out.println(y && x); // False
        System.out.println(y ^ x); // True

    }
}
