package abend9.Arrays;

public class Array {
    public static void main(String args[]){
        int[] arr = {5,3,12,46};
        System.out.println(ArraySum(arr));
    }
    public static int ArraySum(int[] arr){
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        return sum;
    }
}
