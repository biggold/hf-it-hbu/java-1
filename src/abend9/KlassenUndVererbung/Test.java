package abend9.KlassenUndVererbung;

public class Test {
    public static void main(String args[]){
        Tier tier = new Tier(5,5,"blau");
        Katze katze = new Katze(5,5,"blau", 3);
        Maus maus = new Maus(5,5,"blau");
        tier.rufen();
        katze.rufen();
        maus.rufen();

        Maus maus1 = new Maus(1,1,"blau");
        Maus maus2 = new Maus(2,2,"blau");
        Maus maus3 = new Maus(3,3,"blau");
        Maus maus4 = new Maus(4,4,"blau");
        System.out.println("1: " + katze.essen(maus1));
        System.out.println(katze.essen(maus2));
        System.out.println(katze.essen(maus3));
        System.out.println(katze.essen(maus4));
    }
}
