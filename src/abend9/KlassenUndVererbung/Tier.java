package abend9.KlassenUndVererbung;

public class Tier {
    private int gewicht;
    private int alter;
    private String fellFarbe;
    Tier(int gewicht, int alter, String fellFarbe){
        this.gewicht = gewicht;
        this.alter = alter;
        this.fellFarbe = fellFarbe;
    }

    public void rufen(){}


    /*
    Getters and Setters
     */
    public int getGewicht() {
        return gewicht;
    }

    public void setGewicht(int gewicht) {
        this.gewicht = gewicht;
    }

    public int getAlter() {
        return alter;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }

    public String getFellFarbe() {
        return fellFarbe;
    }

    public void setFellFarbe(String fellFarbe) {
        this.fellFarbe = fellFarbe;
    }
}
