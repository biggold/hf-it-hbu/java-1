package abend9.KlassenUndVererbung;

public class Maus extends Tier {
    private String lieblingsKaese;
    Maus(int gewicht, int alter, String fellFarbe){
        super(gewicht,alter,fellFarbe);
    }

    @Override
    public void rufen() {
        System.out.println("Beep");
    }


    /*
    Getters and Setters
    */
    public String getLieblingsKaese() {
        return lieblingsKaese;
    }

    public void setLieblingsKaese(String lieblingsKaese) {
        this.lieblingsKaese = lieblingsKaese;
    }
}
