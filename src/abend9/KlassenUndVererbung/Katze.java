package abend9.KlassenUndVererbung;

import java.util.Arrays;

public class Katze extends Tier {
    private Maus[] magen;
    Katze(int gewicht, int alter, String fellFarbe, int groesseMagen){
        super(gewicht,alter,fellFarbe);
        this.magen = new Maus[groesseMagen];
    }

    @Override
    public void rufen() {
        System.out.println("Miau");
    }

    public int essen(Maus maus) {
        int count = 0;
        for (int i = 0; i < this.magen.length; i++){
            System.out.println(i);
            if (this.magen[i] == null){
                count = i;
                this.magen[i] = maus;
            }
        }
        return count;
    }

    /*
        Getters and Setters
        */
    public Maus[] getMagen() {
        return magen;
    }
    public void setMagen(Maus[] magen) {
        this.magen = magen;
    }
}
