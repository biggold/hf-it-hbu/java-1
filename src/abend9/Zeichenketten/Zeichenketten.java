package abend9.Zeichenketten;

public class Zeichenketten {
    public static String gemeinsameEndung(String s1, String s2){
        int stringIndex = 0;
        for (int i = s1.length(); i > 0; i--){
            if (s2.endsWith(s1.substring(i))){
                stringIndex = i;
            }
        }
        return s1.substring(stringIndex);
    }
    public static void main(String args[]){
        System.out.println(gemeinsameEndung("Tischlerei", "Fischerei"));
    }
}
