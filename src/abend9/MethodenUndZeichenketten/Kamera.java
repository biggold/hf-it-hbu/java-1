package abend9.MethodenUndZeichenketten;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Kamera {
    public static String updateImageFilename(String imageFilename) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        Date now = new Date();
        return sdf.format(now) + imageFilename.substring(3);
    }
}
