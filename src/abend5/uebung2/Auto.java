package abend5.uebung2;

public class Auto {
    private String color;
    private int price;
    private int cubicCapacity;
    private long serialNumber;
    static private int soldCount;
    static private long[] usedSerialNumbers;

    /**
     * Berechnet die nächste Seriennummer und weist diese dem aktuellen Auto zu, aktualisiert die
     * Anzahl verkaufter Autos und aktualisiert die bereits verwendeten Seriennummern
     */
    public void init(){
        this.serialNumber = (long) Math.random();
    }

    /**
     * Gibt alle Instanzvariablen in der Konsole aus
     */
    public void print(){

    }
}
