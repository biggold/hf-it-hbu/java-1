package abend4.fotokamera;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        Fotokamera foto = new Fotokamera(true, "WideAngle", 16);

        System.out.println("Soll der Flash aktiviert sein? (J/N)");
        String isFlash = scanner.next();
        if(isFlash == "J") {
            foto.takePicture(true);
        } else {
            foto.takePicture(false);
        }
        scanner.close();
    }
}
