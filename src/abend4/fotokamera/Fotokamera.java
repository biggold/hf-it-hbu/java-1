package abend4.fotokamera;

public class Fotokamera {

    private double zoom = 0;
    private int resolution = 0;
    private String lense = "";
    private boolean flash = false;

    public Fotokamera(boolean hasFlash, String mountedLense, int supportedResolution) {
        flash = hasFlash;
        lense = mountedLense;
        resolution = supportedResolution;
    }

    public Picture takePicture(boolean availableFlash) {
        //Check if Flash is available on the Device and is active
        if (availableFlash && flash) {
            System.out.println("Take Picture with Flash");
        } else {
            System.out.println("Take Picture without Flash");
        }
        // Picture pic = new Picture();
        return new Picture();
    }

    public int savePicture(int a) {

        return a;
    }
    public int changeSetting(int a) {

        return a;
    }

    public void setFlash(boolean setFlash) {
        flash = setFlash;
    }
    public boolean getFlash() {
        return flash;
    }
}


class Picture {

}
