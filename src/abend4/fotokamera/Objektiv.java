package abend4.fotokamera;

public class Objektiv {
    private int brennweiteMin,brennweiteMax,brennweite;

    public void setFocus(int Min, int Max, int brenn) {
        brennweiteMin = Min;
        brennweiteMax = Max;
        brennweite = brenn;
        System.out.println("Neue BrennweiteMin: " + brennweiteMin);
        System.out.println("Neue BrennweiteMax: " + brennweiteMax);
        System.out.println("Neue Brennweite: " + brennweite);
    }
}
