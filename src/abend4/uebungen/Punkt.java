package abend4.uebungen;

/**
 * Aufgabenstellung
 *
 * Implementiere das Spiel „Zahlenraten“. Nach dem Start soll der Benutzer begrüsst werden und er soll
 * kurz über die Regeln des Spiels informiert werden.
 * Mit der Anweisung int secretNumber = (int) (100 * Math.random() + 1); wird eine Zufallszahl
 * von 1 bis 100 generiert.
 * Der Benutzer soll nun versuchen, diese Zahl zu erraten. Implementiere dazu eine Schleife, die in jedem
 * Durchlauf jeweils
 * 1. informiert,um den wievielten Rateversuch es sich handelt
 * 2. ein Rateversuch eingegeben werden kann
 * 3. informiert, ob die eingegebene Zahl zu gross, zu klein oder korrekt ist
 * Die Schleife wird solange durchlaufen, bis die Zahl erraten ist.
 */

public class Punkt {
    private int xKoordinate;
    private int yKoordinate;

    /**
     * Liefert den Wert der x-Koordinate.
     * Hat keine Übergabeparameter
     * @return
     */
    public int getxKoordinate() {
        return xKoordinate;
    }

    /**
     * Setzt den Wert der x-Koordinate
     * @param xKoordinate
     */
    public void setxKoordinate(int xKoordinate) {
        this.xKoordinate = xKoordinate;
    }
    /**
     * Liefert den Wert der y-Koordinate.
     * Hat keine Übergabeparameter
     * @return
     */
    public int getyKoordinate() {
        return yKoordinate;
    }

    /**
     * Setzt den Wert der y-Koordinate
     * @param yKoordinate
     */
    public void setyKoordinate(int yKoordinate) {
        this.yKoordinate = yKoordinate;
    }

    /**
     * Verschiebt den Punkt in eine bestimmte Richtung.
     * Beispiel:
     *      Der Aufruf verschiebe(3, -2) verschiebt den Punkt um 3 Einheiten in positiver x-Richtung
     *      und um 2 Einheiten in negativer y-Richtung.
     * @param xRichtung
     * @param yRichtung
     */
    public void verschiebe(int xRichtung, int yRichtung){
        setxKoordinate(getxKoordinate() + xRichtung );
        setyKoordinate((getyKoordinate() + yRichtung));
    }
    public void printKoordinaten(String name){
        if (!name.isEmpty()){
            System.out.println("Punkt: " + name);
        }
        System.out.println("X Koordinate: " + getxKoordinate());
        System.out.println("Y Koordinate: " + getyKoordinate());
    }
    public void printKoordinaten(){
        printKoordinaten("");
    }
}
