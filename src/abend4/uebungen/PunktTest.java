package abend4.uebungen;

public class PunktTest {
    public static void main(String[] args){
        Punkt p1 = new Punkt();
        p1.setxKoordinate(3);
        p1.setyKoordinate(0);
        Punkt p2 = new Punkt();
        p2.setxKoordinate(6);
        p2.setyKoordinate(9);

        Punkt p3 = p1;
        p3.printKoordinaten("P3");
        p3 = p2;
        p3.printKoordinaten("P3");
        p3.setxKoordinate(7);
        p2.printKoordinaten("P2");

    }
}
