package abend6.pruefung.strom;

public class OhmischesGesetz {
    private Double strom, wiederstand, leistung;
    private static final double SPANNUNG = 230;

    /**
     * Gibt den Strom zurück
     *
     * @return
     */
    public double getStrom() {
        return strom;
    }

    /**
     * Gibt den Wiederstand zurück
     *
     * @return
     */
    public double getWiederstand() {
        return wiederstand;
    }

    /**
     * Gibt die Leistung zurück
     *
     * @return
     */
    public double getLeistung() {
        return leistung;
    }

    /**
     * Gibt die Spannung zurück
     *
     * @return
     */
    public static double getSPANNUNG() {
        return SPANNUNG;
    }

    public double berechneStrom(double wiederstand) {
        double strom = SPANNUNG / wiederstand;
        this.wiederstand = wiederstand;
        this.strom = strom;
        return strom;
    }

    public double berechneWiederstand(double strom) {
        double wiederstand = SPANNUNG / strom;
        this.wiederstand = wiederstand;
        this.strom = strom;
        return wiederstand;
    }

    public double berechneLeistung() {
        double leistung;
        if (this.strom != null) {
            leistung = SPANNUNG * this.strom;
        } else if (this.wiederstand != null) {
            leistung = Math.pow(SPANNUNG, 2) / this.wiederstand;
        } else {
            leistung = 0;
        }
        this.leistung = leistung;
        return leistung;
    }
}
