package abend6.pruefung.currency;

public class CurrencyConverter {
    /**
     * Die Methode soll den Betrag in Schweizer Franken (CHF) anhand Währungscode in die entsprechende Währung umwandeln.
     * @param amount
     * @param currency
     * @return
     */
    public double konvertiereWaerung(double amount, String currency){
        double calc = 0;
        switch(currency){
            case "EUR":
                calc = amount * 0.9;
                break;
            case "USD":
                calc = amount * 1.06;
                break;
            case "JPY":
                calc = amount * 110.50;
                break;
            case "AUD":
                calc = amount * 1.47;
                break;
            case "GBP":
                calc = amount * 0.8;
                break;
            case "NOK":
                calc = amount * 9.35;
                break;
        }
        return calc;
    }
}


