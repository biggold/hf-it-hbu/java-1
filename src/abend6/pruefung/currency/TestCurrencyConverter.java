package abend6.pruefung.currency;

public class TestCurrencyConverter {
    public static void main(String[] args) {
        CurrencyConverter eur = new CurrencyConverter();
        System.out.println(eur.konvertiereWaerung(10.0,"EUR"));
        System.out.println(eur.konvertiereWaerung(10.0,"USD"));
        System.out.println(eur.konvertiereWaerung(10.0,"JPY"));
        System.out.println(eur.konvertiereWaerung(10.0,"AUD"));
        System.out.println(eur.konvertiereWaerung(10.0,"GBP"));
        System.out.println(eur.konvertiereWaerung(10.0,"NOK"));
    }
}
