package abend6.pruefung.stuffandthings;

public class WhileSchleife {
    public static void main(String[] args) {
        int n = 100;
        int i = 1;
        WhileSchleife(100, 1);
        ForSchleife(100, 1);
        System.out.println("########## NEW TRY ##########");
        WhileSchleife(0, 1);
        ForSchleife(0, 1);
        System.out.println("########## NEW TRY ##########");
        WhileSchleife(10000, 1);
        ForSchleife(10000, 1);


    }

    public static void ForSchleife(int n, int i) {
        boolean prim = true;
        System.out.println("Primzahlen bis " + n + ": ");
        for (i = i; i < n; i++) {
            for (int j = 2; j < i - 1; j++) {
                if (i % j == 0) {
                    prim = false;
                }
            }
            if (prim) {
                System.out.println(i);
            } else {
                prim = true;
            }
        }
    }

    public static void WhileSchleife(int n, int i) {
        boolean prim = true;
        System.out.println("Primzahlen bis " + n + ": ");
        while (i < n) {
            int j = 2;
            while (j < i - 1) {
                if (i % j == 0) {
                    prim = false;
                }
                j++;
            }
            if (prim) {
                System.out.println(i);
            } else {
                prim = true;
            }
            i++;
        }
    }
}
