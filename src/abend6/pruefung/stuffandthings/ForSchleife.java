package abend6.pruefung.stuffandthings;

public class ForSchleife {
    public static void main(String[] args){
        int i = 1;
        while (i < 33000) {
            System.out.println(i);
            i *=2;
        }

        for(int i2 = 1; i2<33000; i2*=2){
            System.out.println(i2);
        }
    }
}
