package abend6.pruefung.stuffandthings;

public class DoWhileSchleife {
    public static void main(String[] args) {
        int i = 0; // testen mit i=0, i=1 und i = 10
        int j = 0;

        do {
            double kreisinhalt = Math.round((Math.PI * Math.pow(j, 2)) * 1000.0) / 1000.0;
            System.out.println("Der Kreisinhalt von " + j + "cm Radius ist " + kreisinhalt + "cm2");
            j = j + 1;
        } while (j < i);

        System.out.println("My version:");

        System.out.println("i = " + i);
        boolean runAtLeatOnce = true;
        for (j = 0; j < i || runAtLeatOnce; j++) {
            runAtLeatOnce = false;
            double kreisinhalt = Math.round((Math.PI * Math.pow(j, 2)) * 1000.0) / 1000.0;
            System.out.println("Der Kreisinhalt von " + j + "cm Radius ist " + kreisinhalt + "cm2");
        }
    }
}
