package abend3.uebung2;

import java.util.Scanner;

/**
 * Aufgabenstellung
 * Schreibe ein Programm, welches eine Ziffer (0-9) von der Konsole einliest und dann die Ziffer in Worten
 * ausgibt. Beispiel: Input 7, Output: „Sieben“. Beim Zeichen ‚e‘ bricht das Programm ab. Sonst wird eine
 * weitere Ziffer eingelesen.
 * Beachte: Ein char kann folgendermassen eingelesen werden:
 *       Mit dem Scanner => char zeichen = scanner.nextLine().charAt(0);
 *         Oder
 *       Mit einem Input Stream
 *         => char zeichenCode = (char) System.in.read();
 *         => Allerdings muss die main() Methode noch eine IOException werfen (behandeln wir erst
 *         später im Kurs)
 */

public class Switch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object
        boolean running = true;
        while (running) {
            System.out.print("Nummer zwischen 0-9 Eingeben oder e für exit: ");
            if (scanner.hasNextLine()) {
                char zeichen = scanner.nextLine().charAt(0);
                switch (zeichen) {
                    case ('0'):
                        System.out.println("Null");
                        break;
                    case ('1'):
                        System.out.println("Eins");
                        break;
                    case ('2'):
                        System.out.println("Zwei");
                        break;
                    case ('3'):
                        System.out.println("Drei");
                        break;
                    case ('4'):
                        System.out.println("Vier");
                        break;
                    case ('5'):
                        System.out.println("Fünf");
                        break;
                    case ('6'):
                        System.out.println("Sechs");
                        break;
                    case ('7'):
                        System.out.println("Sieben");
                        break;
                    case ('8'):
                        System.out.println("Acht");
                        break;
                    case ('9'):
                        System.out.println("Neun");
                        break;
                    case ('e'):
                        System.out.println("Exiting now!");
                        running = false;
                        //System.exit(0);
                        break;
                    default:
                        System.out.println("Zeichen " + zeichen + " nicht erkannt!");
                }
            } else {
                System.out.println("Please enter a value!");
            }
        }

    }
}
