package abend3.uebung2;

/**
 * Aufgabenstellung
 * a)   Schreibe ein Programm, welches alle Zahlen von 1 bis 100 in die Konsole schreibt. Verwende
 * dazu eine FOR Schleife um die Zahlen 1 bis 100 durchzugehen.
 * b)   Schreibe ein Programm, welches alle Zahlen von 1 bis 100 in die Konsole schreibt, die durch 11
 * oder 17 teilbar sind. Verwende dazu eine FOR Schleife um die Zahlen 1 bis 100 durchzugehen.
 * Prüfe jede Zahl auf ihre Teilbarkeit mit dem Modulo % Operator.
 */

public class ForLoop {
    public static void main(String[] args) {
        for (int i = 0; i < 101; i++) {
            System.out.println(i);
        }

        for (int i = 0; i < 101; i++) {
            if (i >= 11 && (i % 11) == 0) {
                System.out.println(i + " ist durch 11 teilbar!");
            }
            if (i >= 17 && (i % 17) == 0) {
                System.out.println(i + " ist durch 17 teilbar!");
            }
        }
    }
}
