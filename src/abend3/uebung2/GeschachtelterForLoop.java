package abend3.uebung2;

import java.util.Scanner;

/**
 * Aufgabenstellung
 *
 * Schreibe ein Programm, das mit *-Zeichen zusammengesetztes Dreieck auf dem Bildschirm ausgibt. Der
 * Benutzer soll zu Beginn nach der Anzahl Zeilen des Dreiecks gefragt werden.
 * Tipp: Verwende geschachtelte for-Anweisungen.
 */

public class GeschachtelterForLoop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object
        System.out.print("Anzahl Zeilen des Dreiecks angeben: ");
        if(scanner.hasNextInt()){
            int lines = scanner.nextInt();
            for(int line=1; line <=lines; line++){
                for(int star=1; star<=line; star++){
                    System.out.print("*");
                }
                System.out.println();
            }
        }

    }
}
