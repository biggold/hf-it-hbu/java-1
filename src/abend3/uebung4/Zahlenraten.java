package abend3.uebung4;

import java.util.Scanner;

/**
 * Aufgabenstellung
 *
 * Implementiere das Spiel „Zahlenraten“. Nach dem Start soll der Benutzer begrüsst werden und er soll
 * kurz über die Regeln des Spiels informiert werden.
 * Mit der Anweisung int secretNumber = (int) (100 * Math.random() + 1); wird eine Zufallszahl
 * von 1 bis 100 generiert.
 * Der Benutzer soll nun versuchen, diese Zahl zu erraten. Implementiere dazu eine Schleife, die in jedem
 * Durchlauf jeweils
 * 1. informiert,um den wievielten Rateversuch es sich handelt
 * 2. ein Rateversuch eingegeben werden kann
 * 3. informiert, ob die eingegebene Zahl zu gross, zu klein oder korrekt ist
 * Die Schleife wird solange durchlaufen, bis die Zahl erraten ist.
 */

public class Zahlenraten {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object
        System.out.println("*** Zahlenraten ***");

        System.out.println("Ich denke mir eine Zahl zwischen 1 und 100. Errate diese Zahl!");
        int secretNumber = (int) (100 * Math.random() + 1);
        int userNumber = -1;
        for (int counter = 1; userNumber != secretNumber; counter++) {
            System.out.print(counter + ". Versuch: ");
            userNumber=scanner.nextInt();
            if (userNumber > secretNumber) {
                System.out.println("Meine Zahl ist kleiner!");
            } else if (userNumber < secretNumber) {
                System.out.println("Meine Zahl ist grösser!");
            } else {
                System.out.println("Du hast meine Zahl beim "+ counter +". Versuch erraten!");
            }
        }
    }
}
