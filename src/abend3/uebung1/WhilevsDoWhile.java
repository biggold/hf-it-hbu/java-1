package abend3.uebung1;

/** Aufgabenstellung
 * Was gibt das folgende Stück Java-Code aus? Bei a=1? Bei a=10? Zur Überprüfung codiere das Beispiel
 *   a)
 *         int a = 1;
 *         do {
 *             System.out.println(a);
 *             a++;
 *         } while ( a < 10);
 *
 *
 *   b)
 *         int a = 1;
 *         while (a < 10) {
 *             System.out.println(a);
 *             a++;
 *         }
 *
 */

public class WhilevsDoWhile {
    public static void main(String[] args){
        System.out.println("a)");
        int a = 1;
        do {
            System.out.println(a);
            a++;
        } while ( a < 10);
        a = 10;
        do {
            System.out.println(a);
            a++;
        } while ( a < 10);
        System.out.println("b)");
        a = 1;
        while (a < 10) {
            System.out.println(a);
            a++;
        }
        a = 10;
        while (a < 10) {
            System.out.println(a);
            a++;
        }
    }
}
