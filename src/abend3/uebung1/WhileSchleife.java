package abend3.uebung1;

/** Aufgabenstellung
 * Was gibt das folgende Stück Java-Code aus? Zur Überprüfung codiere das Beispiel.
 *   a)
 *         int i = 1;
 *         int a = 0;
 *         while (i < 10) {
 *             a = a + i;
 *             i = i + 1;
 *             System.out.println(a);
 *         }
 *   b)
 *         int i = 1;
 *         while (i < 101) {
 *             System.out.println(i);
 *             i++;
 *         }
 *
 */

public class WhileSchleife {
    public static void main (String[] args){
        System.out.println("a)");
        int i = 1;
        int a = 0;
        while (i < 10) {
            a = a + i;
            i = i + 1;
            System.out.println(a);
        }
        System.out.println("b)");
        i = 1;
        while (i < 101) {
            System.out.println(i);
            i++;
        }
    }
}
