package abend3.uebung3;

import java.util.Scanner;

/**
 * Aufgabenstellung
 *
 * Implementiere den GGT Algorithmus gemäss folgendem NSD (Nassi-Shneiderman- Diagramm). Lese die
 * Zahlen x und y aus der Konsole und gib das Resultat ebenfalls in die Konsole zurück.
 *
 * BILD: NSD-Diagram.jpg
 *
 * Beachte: Einen Zahlenwert von der Konsole kann mit der Scanner-Klasse eingelesen werden.
 * Scanner scannerInput = new Scanner(System.in);
 * // liest eine Ganzzahl aus der Konsole und weist den Wert x zu
 * x = scannerInput.nextInt();
 *
 * Lagere die Berechnung des GGT in eine eigene Methode aus.
 * Die neue Methode hat folgende Signatur:
 * private static int BerechneGGT(int x, int y)
 */

public class GGTBerechnungWithMethod {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object

        int x = 0, y = 0;
        System.out.print("x angeben: ");
        if (scanner.hasNextInt()) {
            x = scanner.nextInt();
        }
        System.out.print("y angeben: ");
        if (scanner.hasNextInt()) {
            y = scanner.nextInt();
        }
        System.out.println("x: " + x + " y: " + y);
        System.out.println(BerechneGGT(x,y) + " ist GGT");
    }
    private static int BerechneGGT(int x, int y){
        while (x != y) {
            if (x < y) {
                y = y - x;
            } else {
                x = x - y;
            }
        }
        return x;
    }
}
